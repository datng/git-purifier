# Development
- Add automated tests
- Add install step

# Interface
- Use the name of the working directory as root when creating a gpDirectoryTree
- Add a scrollbar to the delete big files dialog

# Safety
- Add warning and confirmation dialog when rewriting git history.

