if [ -d "dummy" ]; then
    rm -rf dummy
fi

mkdir dummy
cd dummy

git init
git config user.name "dummy"
git config user.email "dummy@dummy.address"
# create simple commit
touch README.md
git add README.md
git commit -m "add README"

# big file
dd if=/dev/zero of=big_file_50MiB.blob count=51200 bs=1024
git add big_file_50MiB.blob
git commit -m "add blob"

# file with spaces in name
touch "spacey file.txt"
git add "spacey file.txt"
git commit -m "add spacey file"

# remove big file the dirty way
rm big_file_50MiB.blob
git add big_file_50MiB.blob
git commit -m "remove blob"

# rename file
echo "some text" > text.txt
git add text.txt
git commit -m "add text"
echo "some more text" >> text.txt
git add text.txt
git commit -m "add more text"
mv text.txt moved.txt
git add text.txt moved.txt
git commit -m "move text"
echo "some more text to the moved file" >> moved.txt
git add moved.txt
git commit -m "add text to the moved file"

# add file with special characters
echo "try to remove me" > "I'm a special file, right, \"friend\"?"
git add "I'm a special file, right, \"friend\"?"
git commit -m "add file with special name"

# some nested folders
mkdir dir1 && cd dir1 && mkdir dir1.1 && mkdir dir1.2 && cd ..
mkdir dir2 && cd dir2 && mkdir dir2.1 && mkdir dir2.2 && cd ..
echo "text for dir 1.1" > "dir1/dir1.1/text.txt"
echo "text for dir 1.2" > "dir1/dir1.2/text.txt"
echo "text for dir 2.1" > "dir2/dir2.1/text.txt"
echo "text for dir 2.2" > "dir2/dir2.2/text.txt"
git add .
git commit -m "add folders"
