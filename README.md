# Git Purifier
## Purpose
Git Purifier is a way to rewrite git history with the help of a graphical user interface.

The emphasis of this project is on its graphical user interface. If you prefer a command line tool for such a task, this is not for you.

## Features
Git Purifier allows users to:
- [x] Fix commit author names and emails
- [x] Remove big files
- [x] Remove whole folders
- [ ] Replace sensitive texts

without having to copy and paste obscure scripts from the Internet, or unwarranted political messages during usage.

## Dependencies
- Git Purifier has never been tested on Windows, since Bash is required for the shell scripts.
- The lowest tested version of git is 2.16.4 on OpenSUSE Leap 15.0
- Any Linux distribution with Qt 5 should work. The lowest tested version of Qt is 5.9.4 on OpenSUSE Leap 15.0
- Setup
  - On OpenSUSE: `sudo zypper install --no-recommended cmake git libqt5-qtbase-devel`
  - On Ubuntu: `sudo apt update && sudo apt install cmake git g++ make qt5-default`

## Compilation
```
mkdir build
cd build
cmake ..
make -j$(nproc)
```

## Usage
The compiled program can be executed by running `./gpexec` from the build directory.

