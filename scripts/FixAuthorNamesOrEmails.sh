for i in "$@"; do
    case $i in
    --old-email=*)
        export OLD_EMAIL="${i#*=}"
        ;;
    --old-name=*)
        export OLD_NAME="${i#*=}"
        ;;
    --new-email=*)
        export NEW_EMAIL="${i#*=}"
        ;;
    --new-name=*)
        export NEW_NAME="${i#*=}"
        ;;
    -f)
        OVERWRITE_BACKUP_FLAG="-f"
        ;;
    esac
done

if [[ -z ${OLD_EMAIL} ]]; then
    >&2 echo "No old email given."
    exit 1;
fi

if [[ -z ${OLD_NAME} ]]; then
    >&2 echo "No old name given."
    exit 1;
fi

if [[ -z ${NEW_EMAIL} ]]; then
    >&2 echo "No new email given."
    exit 1;
fi

if [[ -z ${NEW_NAME} ]]; then
    >&2 echo "No new name given."
    exit 1;
fi

git filter-branch $OVERWRITE_BACKUP_FLAG --env-filter '
echo $OLD_EMAIL $OLD_NAME $NEW_EMAIL $NEW_NAME
WRONG_EMAIL="$OLD_EMAIL"
WRONG_NAME="$OLD_NAME"
CORRECT_EMAIL="$NEW_EMAIL"
CORRECT_NAME="$NEW_NAME"

if [ "$GIT_COMMITTER_EMAIL" = "$WRONG_EMAIL" ]; then
    export GIT_COMMITTER_NAME="$CORRECT_NAME"
    export GIT_COMMITTER_EMAIL="$CORRECT_EMAIL"
fi

if [ "$GIT_COMMITTER_NAME" = "$WRONG_NAME" ]; then
    export GIT_COMMITTER_NAME="$CORRECT_NAME"
    export GIT_COMMITTER_EMAIL="$CORRECT_EMAIL"
fi

if [ "$GIT_AUTHOR_EMAIL" = "$WRONG_EMAIL" ]; then
    export GIT_AUTHOR_NAME="$CORRECT_NAME"
    export GIT_AUTHOR_EMAIL="$CORRECT_EMAIL"
fi

if [ "$GIT_AUTHOR_NAME" = "$WRONG_NAME" ]; then
    export GIT_AUTHOR_NAME="$CORRECT_NAME"
    export GIT_AUTHOR_EMAIL="$CORRECT_EMAIL"
fi
' --tag-name-filter cat -- --branches --tags
