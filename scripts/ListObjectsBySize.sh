#!/bin/bash

for i in "$@"; do
    case $i in
    --config-location=*) # specify config location
        CONFIG_LOCATION="${i#*=}"
        ;;
    esac
done

if [[ -z ${CONFIG_LOCATION} ]]; then
    >&2 echo "CONFIG_LOCATION is not set."
    exit 1
fi

REVLISTFILE="$CONFIG_LOCATION/revision-list.txt"
>$REVLISTFILE # clear the file if it already exists

for commitSHA1 in $(git rev-list --all); do
    git ls-tree -r --long $commitSHA1 >> $REVLISTFILE
done

# sort by SHA1, remove duplicate entries, then sort by size
sort --key 3 "$REVLISTFILE" | \
uniq | \
sort --key 4 --numeric-sort --reverse

rm $REVLISTFILE
