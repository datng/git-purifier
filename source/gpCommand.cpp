﻿/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpCommand.cpp
 * @brief This file contains the implementation of the gpCommand class.
 */

#include "gpCommand.h"

#include <QApplication>

gpCommand::gpCommand() = default;

auto gpCommand::CountObjects() -> gpCommand {
    gpCommand command;
    command.Program = "git";
    command.Arguments //
        << "count-objects"
        << "-vH";
    return command;
}
auto gpCommand::FixAuthorNamesOrEmails( //
    const QString &eOldEmail,
    const QString &eOldName,
    const QString &eNewEmail,
    const QString &eNewName) -> gpCommand {
    gpCommand command;
    command.Program = "bash";
    command.Arguments << "-e" << QCoreApplication::applicationDirPath() + "/scripts/FixAuthorNamesOrEmails.sh" //
                      << "--old-email=" + eOldEmail //
                      << "--old-name=\"" + eOldName + "\"" //
                      << "--new-email=" + eNewEmail //
                      << "--new-name=\"" + eNewName + "\"" //
                      << "-f";
    return command;
}

auto gpCommand::ListAuthors() -> gpCommand {
    gpCommand command;
    command.Program = "bash";
    command.Arguments << "-e" << QCoreApplication::applicationDirPath() + "/scripts/ListAuthors.sh";
    return command;
}

auto gpCommand::ListObjectsByName(const QDir &eConfigLocation) -> gpCommand {
    gpCommand command;
    command.Program = "bash";
    command.Arguments << "-e" << QCoreApplication::applicationDirPath() + "/scripts/ListObjectsByName.sh"
                      << "--config-location=" + eConfigLocation.path();
    return command;
}

auto gpCommand::ListObjectsBySize(const QDir &eConfigLocation) -> gpCommand {
    gpCommand command;
    command.Program = "bash";
    command.Arguments << "-e" << QCoreApplication::applicationDirPath() + "/scripts/ListObjectsBySize.sh"
                      << "--config-location=" + eConfigLocation.path();
    return command;
}

auto gpCommand::RemoveDirectory(const QString &ePath) -> gpCommand {
    QString path = "";
    for (auto i : ePath) {
        auto c = i.toLatin1();
        if ('\"' == c) {

            path.append('\\');
            path.append('\"');
        } else {
            path.append(c);
        }
    }

    gpCommand command;
    command.Program = "bash";
    command.Arguments //
        << "-e" //
        << QCoreApplication::applicationDirPath() + "/scripts/RemoveDirectory.sh" //
        << '\"' + path + '\"';

    return command;
}

auto gpCommand::RemoveFile(const QString &eFileName) -> gpCommand {
    // append escape characters so the script can parse
    QString filename = "";
    for (auto i : eFileName) {
        auto c = i.toLatin1();
        switch (c) {
        case '\"':
            filename.append('\\');
            filename.append('\"');
            break;
        default:
            filename.append(c);
        }
    }
    gpCommand command;
    command.Program = "bash";
    command.Arguments //
        << "-e" //
        << QCoreApplication::applicationDirPath() + "/scripts/RemoveFile.sh" //
        << '\"' + filename + '\"';
    return command;
}
