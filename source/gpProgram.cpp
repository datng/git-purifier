#include "gpProgram.h"

#include "gpCommon.h"
#include "gpUtils.h"

#include <QFile>

#include <iostream>

namespace gp {
gpProgram::gpProgram() {
    auto dir = gpConfigDir();
    if (!dir.exists()) {
        std::cout << "The config path " << dir.path().toStdString() << " does not exist. Creating..." << std::endl;
        dir.mkpath(".");
    }
    InitializeWorkingDirectory();
}

auto gpProgram::WorkingDirectory() const -> QDir {
    return m_WorkingDirectory;
}

void gpProgram::SetWorkingDirectory(const QDir &eDir) {
    m_WorkingDirectory = eDir;
}

void gpProgram::SaveWorkingDirectory(const QString& eWorkingDirectory) {
    QDir configDir = gpConfigDir();
    QFile file(configDir.filePath(CONFIGFILE_WORKING_DIRECTORY));
    file.open(QFile::OpenModeFlag::Truncate | QFile::OpenModeFlag::WriteOnly);
    file.write(eWorkingDirectory.toStdString().c_str());
    file.close();
}

void gpProgram::InitializeWorkingDirectory() {
    auto configDir = gpConfigDir();
    if (configDir.exists(CONFIGFILE_WORKING_DIRECTORY)) {
        QFile file(configDir.filePath(CONFIGFILE_WORKING_DIRECTORY));
        file.open(QFile::OpenModeFlag::ReadOnly);
        auto fileContent = file.readAll();
        QDir workingDir(fileContent);
        if (workingDir.exists()) { m_WorkingDirectory = workingDir; }
        file.close();
    } else {
        QFile temp(configDir.filePath(CONFIGFILE_WORKING_DIRECTORY));
        std::cout << temp.fileName().toStdString() << " does not exist." << std::endl;
    }
}
} // namespace gp
