/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpObjectInfo.cpp
 * @brief This file contains the implementation of the gpObjectInfo class.
 */

#include "gpObjectInfo.h"

#include <QFile>

#include <iostream>
#include <utility>

gpObjectInfo::gpObjectInfo(QString ePath, int eSize) : Path(std::move(ePath)), Size(eSize) {}

auto gpObjectInfo::Parse(const QString &eWorkingDirectory, const QString &eString) -> gpObjectInfo {
    int index = 0;
    QString sizeString("");
    QString pathString("");

    // skip through unused information: mode, type, object
    while (eString.at(index) != ' ') {
        ++index; // mode
    }
    ++index; // the ' ' after mode
    while (eString.at(index) != ' ') {
        ++index; // type
    }
    ++index; // the ' ' after type
    while (eString.at(index) != ' ') {
        ++index; // object
    }

    while (eString.at(index) == ' ') {
        ++index; // a bunch of spaces after object
    }

    // copy size and file to create gpObjectInfo object
    while (eString.at(index) != '\t') {
        sizeString += eString.at(index);
        ++index;
    }
    ++index; // the tab after size
    while (index < eString.size()) {
        pathString += eString.at(index);
        ++index;
    }
    pathString = pathString.split("\n")[0]; // get rid of the line break

    // file name is wrapped inside quotes
    if (pathString[0] == '\"' && pathString[pathString.size() - 1] == '\"') {
        if (QFile(eWorkingDirectory + "/" + pathString).exists()) {
            // is ok
        } else {
            auto filewithoutquotes = pathString;
            // remove quotes
            filewithoutquotes.remove(0, 1);
            filewithoutquotes.remove(filewithoutquotes.size() - 1, 1);

            filewithoutquotes.remove("\\");
            if (QFile(eWorkingDirectory + "/" + filewithoutquotes).exists()) {
                pathString = filewithoutquotes;
            } else {
                TODO; // some error handling needed for this situation
                std::cerr << (eWorkingDirectory + "/" + filewithoutquotes).toStdString() << " does not exist."
                          << std::endl;
            }
        }
    }

    return gpObjectInfo(pathString, sizeString.toInt());
}
