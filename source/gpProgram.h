#ifndef GPPROGRAM_H
#define GPPROGRAM_H
#include "gpCommon.h"

#include <QDir>

namespace gp {
class gpProgram {
public:
    gpProgram(); //!< The default constructor.
    QDir WorkingDirectory() const;
    void SetWorkingDirectory(const QDir &eDir);

    static void SaveWorkingDirectory(const QString&);

private:
    /**
     * @brief InitializeWorkingDirectory
     * Load the last session's working directory if it was saved.
     * Otherwise, use the directory in which git-purifier was called.
     */
    void InitializeWorkingDirectory();
    QDir m_WorkingDirectory;
};
} // namespace gp
#endif // GPPROGRAM_H
