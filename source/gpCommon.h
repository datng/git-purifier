/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpCommon.h
 * @brief This file contains the common definitions that can used throughout the project.
 */

#ifndef GPCOMMON_H
#define GPCOMMON_H
#include <QString>

/**
 * @brief TODO is a trigger for incomplete code to issue a warning during compilation.
 */
#define TODO \
    { bool unfinished = true; }

// all referenced namespaces
namespace gp {
namespace gui {}
const inline static char *CONFIGFILE_WORKING_DIRECTORY("working_directory.txt");
} // namespace gp

/**
 * @brief contains all UI elements
 */
namespace Ui {} // namespace Ui

#endif // GPCOMMON_H
