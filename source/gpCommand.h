﻿/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpCommand.h
 * @brief This file contains the declaration of the gpCommand class.
 */

#ifndef GPCOMMAND_H
#define GPCOMMAND_H

#include <QDir>
#include <QString>
#include <QStringList>

/**
 * @brief The gpCommand class describes a command that can be used by a QProcess.
 *
 * The command is the combination of a program name and its accompanying command line arguments.
 */
class gpCommand {
public:
    gpCommand(); ///< The default constructor

    /**
     * @brief CountObjects generates the command to count objects in a git repository
     * in a verbose and human-readable way.
     * @return the generated command.
     */
    static gpCommand CountObjects();

    static gpCommand FixAuthorNamesOrEmails( //
        const QString &eOldEmail,
        const QString &eOldName,
        const QString &eNewEmail,
        const QString &eNewName);

    static gpCommand ListAuthors();

    /**
     * @brief ListObjectsByName generates the command to list objects in a git repository by name.
     * @return the generated command.
     */
    static gpCommand ListObjectsByName(const QDir& eConfigLocation);

    /**
     * @brief ListObjectsBySize generates the command to list objects in a git repository by size.
     * @return the generated command.
     */
    static gpCommand ListObjectsBySize(const QDir& eConfigLocation);

    /**
     * @brief RemoveDirectory generates the command to remove a specific directory from a git repository.
     * @param ePath is the path to the to-be-removed directory.
     * @return the generated command.
     */
    static gpCommand RemoveDirectory(const QString &ePath);

    /**
     * @brief RemoveFile generates the command to remove a specific file from a git repository.
     * @param eFileName is the file to be removed.
     * @return the generated command.
     */
    static gpCommand RemoveFile(const QString &eFileName);

    QString Program; ///< The program name.
    QStringList Arguments; ///< The list of command line arguments.
};

#endif // GPCOMMAND_H
