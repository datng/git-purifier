/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpDirectoryTree.h
 * @brief This file contains the declaration of the gpDirectoryTree class.
 */

#ifndef GPDIRECTORYTREE_H
#define GPDIRECTORYTREE_H
#include "gui/gpWidget_DirectoryTree.h" // TODO: fix relationship between these classes

#include <QSet>
#include <QString>

#include <memory>

/**
 * @brief The gpDirectoryTree class describes a directory tree.
 */
class gpDirectoryTree : public QObject {
    Q_OBJECT
public:
    /**
     * @brief gpDirectoryTree is the default constructor.
     * @param eName is the name of the current subtree.
     * @param eParent is the parent of the current branch. The root directory has no parent.
     */
    gpDirectoryTree(QString eName = "root", gpDirectoryTree *eParent = nullptr);
    ~gpDirectoryTree(); ///< The default destructor.

    void Clear();

    /**
     * @brief CountItems counts the number of subdirectories inside a given directory, including itself.
     * @return the total number of subdirectories.
     */
    int CountItems();
    QString Path() const; ///< Returns the full path relative to the root node.
    QString Print(); ///< Prints the tree and returns the printed string.
    void ShowWidgets(); ///< Shows the widgets associated with the tree's subtrees.
    void SortAlphabetical(); ///< Sorts the tree alphabetically.

    /**
     * @brief UpdateWidgetsContent updates the text content and placement in all widgets
     * associated with the current subtree.
     * @param eParent is the parent widget in which the whole subtree is shown.
     */
    void UpdateWidgetsContent(QWidget *eParent);

    /**
     * @brief UpdateWidgetsPlacement updates the vertical offset of all widgets relative to the parent widget.
     * @param eOffset is the vertical offset.
     */
    void UpdateWidgetsPlacement(int &eOffset);

    QList<std::shared_ptr<gpDirectoryTree>> SubTrees; ///< The list of subtrees.
    unsigned int Level = 0; ///< The level of the current subtree, with 0 being root.
    QString Name = "root"; ///< The name of the current subtree.
    gpDirectoryTree *Parent = nullptr; ///< The parent of the current subtree.
    /**
     * @brief Widget is the widget associated with this node in the tree.
     * It is shown in the Remove Directories dialog.
     * @sa gpDialog_RemoveDirectories
     * @sa ShowWidgets
     */
    gpWidget_DirectoryTree *Widget = nullptr;

public slots:
    void UpdateCheckState(int eCheckState, bool propagateUp, bool propagateDown);
};

#endif // GPDIRECTORYTREE_H
