/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpObjectInfo.h
 * @brief This file contains the declaration of the gpObjectInfo class.
 */

#ifndef GPOBJECTINFO_H
#define GPOBJECTINFO_H
#include "gpCommon.h"

#include <QString>

/**
 * @brief The gpObjectInfo class describes an object in the list of objects returned by the ListObjectsBySize script.
 */
class gpObjectInfo {
public:
    /**
     * @brief gpObjectInfo is the default constructor.
     * @param ePath is the path to the object.
     * @param eSize is the size of the object.
     */
    gpObjectInfo(QString ePath = "", int eSize = 0);

    /**
     * @brief Parse parses the result of the script "ListObjectsBySize.sh" into an gpObjectInfo.
     * Each line has the format:
     * <mode> SP <type> SP <object> SP <size> TAB <file>
     * For more info, see: https://git-scm.com/docs/git-ls-tree
     * @param eString one line from the output of the command.
     * @return an gpObjectInfo corresponding to eString.
     */
    static gpObjectInfo Parse(const QString &eWorkingDirectory, const QString &eString);

    QString Path; ///< The path to the object.
    int Size; ///< The size of the object.
};

#endif // GPOBJECTINFO_H
