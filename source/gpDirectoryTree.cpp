/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpDirectoryTree.cpp
 * @brief This file contains the implementation of the gpDirectoryTree class.
 */

#include "gpDirectoryTree.h"

#include <iostream>
#include <utility>

gpDirectoryTree::gpDirectoryTree(QString eName, gpDirectoryTree *eParent) : Name(std::move(eName)), Parent(eParent) {
    if (Parent != nullptr) { Level = Parent->Level + 1; }
}

gpDirectoryTree::~gpDirectoryTree() {
    Clear();
}

void gpDirectoryTree::Clear() {
    while (!SubTrees.isEmpty()) {
        SubTrees[0]->Clear();
        SubTrees.pop_front();
    }
}

auto gpDirectoryTree::CountItems() -> int {
    if (SubTrees.isEmpty()) { return 1; }
    int count = 1;
    for (auto &child : SubTrees) { count += child->CountItems(); }
    return count;
}

auto gpDirectoryTree::Path() const -> QString {
    if (Level == 0) { return QString(""); }

    QString path = Name;
    if (Parent != nullptr) { path = Parent->Path() + path + "/"; }
    return path;
}

auto gpDirectoryTree::Print() -> QString {
    QString temp;
    for (unsigned int i = 0; i < Level; ++i) {
        if (i == Level - 1) {
            temp += "-> ";
            std::cout << "-> ";
        } else {
            temp += "   ";
            std::cout << "   ";
        }
    }
    temp += Name + "\n";
    std::cout << Name.toStdString() << std::endl;
    for (auto &branch : SubTrees) { temp += branch->Print(); }
    return temp;
}

void gpDirectoryTree::ShowWidgets() {
    Widget->show();
    for (auto &child : SubTrees) { child->ShowWidgets(); }
}

void gpDirectoryTree::SortAlphabetical() {
    // sort complexity not a problem for directory trees
    for (int i = 0; i < SubTrees.size(); ++i) {
        for (int j = i; j < SubTrees.size(); ++j) {
            if (SubTrees[i]->Name > SubTrees[j]->Name) {
                auto temp = SubTrees[i];
                SubTrees[i] = SubTrees[j];
                SubTrees[j] = temp;
            }
        }
    }
}

void gpDirectoryTree::UpdateCheckState(int eCheckState, bool propagateUp, bool propagateDown) {
    auto state = static_cast<Qt::CheckState>(eCheckState);
    switch (state) {
    case Qt::CheckState::Checked:
        if (propagateDown) {
            for (auto &child : SubTrees) {
                child->Widget->SetCheckState(Qt::CheckState::Checked);
                child->UpdateCheckState(Qt::CheckState::Checked, false, true);
            }
        }
        if (propagateUp) {
            if (Parent != nullptr) {
                bool partial = false;
                for (auto &child : Parent->SubTrees) {
                    if (child->Widget->CheckState() != Qt::CheckState::Checked) { partial = true; }
                }
                if (partial) {
                    Parent->Widget->SetCheckState(Qt::CheckState::PartiallyChecked);
                    Parent->UpdateCheckState(Qt::CheckState::PartiallyChecked, true, false);
                } else {
                    Parent->Widget->SetCheckState(Qt::CheckState::Checked);
                    Parent->UpdateCheckState(Qt::CheckState::Checked, true, false);
                }
            }
        }
        break;

    case Qt::CheckState::Unchecked:
        if (propagateDown) {
            for (auto &child : SubTrees) {
                child->Widget->SetCheckState(Qt::CheckState::Unchecked);
                child->UpdateCheckState(Qt::CheckState::Unchecked, false, true);
            }
        }
        if (propagateUp) {
            if (Parent != nullptr) {
                bool partial = false;
                for (auto &child : Parent->SubTrees) {
                    if (child->Widget->CheckState() != Qt::CheckState::Unchecked) { partial = true; }
                }
                if (partial) {
                    Parent->Widget->SetCheckState(Qt::CheckState::PartiallyChecked);
                    Parent->UpdateCheckState(Qt::CheckState::PartiallyChecked, true, false);
                } else {
                    Parent->Widget->SetCheckState(Qt::CheckState::Unchecked);
                    Parent->UpdateCheckState(Qt::CheckState::Unchecked, true, false);
                }
            }
        }
        break;

    case Qt::CheckState::PartiallyChecked:
        if (propagateUp) {
            if (Parent != nullptr) {
                Parent->Widget->SetCheckState(Qt::CheckState::PartiallyChecked);
                Parent->UpdateCheckState(Qt::CheckState::PartiallyChecked, true, false);
            }
        }
        break;
    }
}

void gpDirectoryTree::UpdateWidgetsContent(QWidget *eParent) {
    delete Widget;
    Widget = new gpWidget_DirectoryTree(Name, Level, eParent);

    connect(Widget, &gpWidget_DirectoryTree::CheckBoxClicked, this, &gpDirectoryTree::UpdateCheckState);
    for (auto &child : SubTrees) { child->UpdateWidgetsContent(eParent); }
}

void gpDirectoryTree::UpdateWidgetsPlacement(int &eOffset) {
    Widget->setGeometry( //
        Widget->x(),
        eOffset,
        Widget->width(),
        Widget->height());
    const int entryHeight(30);
    eOffset += entryHeight;
    for (auto &child : SubTrees) { child->UpdateWidgetsPlacement(eOffset); }
}
