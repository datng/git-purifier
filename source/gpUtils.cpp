#include "gpUtils.h"

#include <QStandardPaths>

namespace gp {
auto gpConfigDir() -> QDir {
    QString configLocationString = QStandardPaths::locate( //
        QStandardPaths::ConfigLocation,
        QString(),
        QStandardPaths::LocateDirectory);
    return configLocationString.append("git-purifier");
}
} // namespace gp
