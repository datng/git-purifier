#include "gpAuthorInfo.h"

#include <QStringList>
#include <utility>

auto GetEmail(const QString /*eNameAndEmail*/ &) -> QString;
auto GetName(QString /*eNameAndEmail*/) -> QString;
auto IsValid(const QString /*eNameAndEmail*/ &) -> bool;

auto GetName(QString eNameAndEmail) -> QString {
    if (IsValid(eNameAndEmail)) {
        eNameAndEmail.resize(eNameAndEmail.indexOf("<") - 1);
        return eNameAndEmail;
    }
    return "";
}

auto GetEmail(const QString &eNameAndEmail) -> QString {
    if (IsValid(eNameAndEmail)) {
        QStringList temp = eNameAndEmail.split("<");
        temp[1] = temp[1].remove(">");
        return temp[1];
    }
    return "";
}

auto IsValid(const QString &eNameAndEmail) -> bool {
    if (!eNameAndEmail.contains("<")) { return false; }
    if (!eNameAndEmail.contains(">")) { return false; }
    if (eNameAndEmail.count("<") != 1) { return false; }
    if (eNameAndEmail.count(">") != 1) { return false; }
    return true;
}

gpAuthorInfo::gpAuthorInfo(const QString &eNameAndEmail)
    : m_Name(GetName(eNameAndEmail)), m_Email(GetEmail(eNameAndEmail)) {}

gpAuthorInfo::gpAuthorInfo(QString eName, QString eEmail) : m_Name(std::move(eName)), m_Email(std::move(eEmail)) {}

auto gpAuthorInfo::operator=(const gpAuthorInfo &other) -> gpAuthorInfo & = default;

auto gpAuthorInfo::operator==(gpAuthorInfo &other) const -> bool {
    return m_Name == other.m_Name && m_Email == other.m_Email;
}

auto gpAuthorInfo::Name() const -> QString {
    return m_Name;
}
auto gpAuthorInfo::Email() const -> QString {
    return m_Email;
}

auto gpAuthorInfo::IsValid() const -> bool {
    return m_Name != "" && m_Email != "";
}
