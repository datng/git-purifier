#ifndef GPUTILS_H
#define GPUTILS_H
#include <QDir>

namespace gp {
QDir gpConfigDir();
} // namespace gp

#endif // GPUTILS_H
