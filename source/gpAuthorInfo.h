#ifndef GPAUTHORINFO_H
#define GPAUTHORINFO_H
#include <QString>

class gpAuthorInfo {
public:
    explicit gpAuthorInfo(const QString &eNameAndEmail);
    gpAuthorInfo(QString eName, QString eEmail);
    gpAuthorInfo &operator=(const gpAuthorInfo &other);

    bool operator==(gpAuthorInfo &other) const;
    bool IsValid() const;
    QString Name() const;
    QString Email() const;

    QString m_Name;
    QString m_Email;
};

#endif // GPAUTHORINFO_H
