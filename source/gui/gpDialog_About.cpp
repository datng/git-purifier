/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpDialog_About.cpp
 * @brief This file contains the implementation of the gpDialog_About class.
 */

#include "gpDialog_About.h"
#include "ui_gpDialog_About.h"

#include <QDebug>
#include <QDir>
#include <QFile>

namespace gp::gui {
gpDialog_About::gpDialog_About(QWidget *parent) : QDialog(parent), ui(new Ui::gpDialog_About) {
    ui->setupUi(this);
    auto dir = QDir::currentPath();
    QFile file(dir + "/LICENSE");
    file.open(QIODevice::ReadOnly);
    auto text = file.readAll();
    file.close();

    QFont font("Courier");
    font.setStyleHint(QFont::StyleHint::TypeWriter);

    ui->textBrowser->setFont(font);
    ui->textBrowser->setText(text);
    ui->textBrowser->updateGeometry();

    const int marginWidthBothSides(20);
    int textBrowserWidth = static_cast<int>(ui->textBrowser->document()->idealWidth()) + marginWidthBothSides;
    this->setFixedWidth(textBrowserWidth + marginWidthBothSides);
    ui->textBrowser->setFixedWidth(textBrowserWidth);

    int buttonWidth = ui->pushButton->width();
    const int buttonPosX((textBrowserWidth + 20) / 2 - buttonWidth / 2);
    ui->pushButton->setGeometry( //
        buttonPosX,
        ui->pushButton->y(),
        ui->pushButton->width(),
        ui->pushButton->height());
}

gpDialog_About::~gpDialog_About() {
    delete ui;
}
} // namespace gp::gui
