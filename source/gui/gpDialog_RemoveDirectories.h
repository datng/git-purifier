/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpDialog_RemoveDirectories.h
 * @brief This file contains the declaration of the gpDialog_RemoveDirectories class.
 */

#ifndef GPDIALOG_REMOVEDIRECTORIES_H
#define GPDIALOG_REMOVEDIRECTORIES_H
#include <QDialog>

#include "gpCommon.h"
#include "gpDirectoryTree.h"

#include <QProcess>

namespace Ui {
class gpDialog_RemoveDirectories;
}

/**
 * @brief The gpDialog_RemoveDirectories class describes the Remove Directories dialog.
 */
class gpDialog_RemoveDirectories : public QDialog {
    Q_OBJECT

public:
    /**
     * @brief gpDialog_RemoveDirectories is the default constructor.
     * @param eWorkingDirectory is the directory in which the ListObjectsByName command will be executed.
     * @sa gpCommand::ListObjectsByName()
     */
    explicit gpDialog_RemoveDirectories(const QString &eWorkingDirectory);
    ~gpDialog_RemoveDirectories() override; ///< The default destructor.

protected:
    void reject() override;
    void resizeEvent(QResizeEvent *eEvent) override;

private slots:
    void on_pushButton_Remove_clicked();

    static void ReadError_ListDirectoriesProcess();
    static void ReadError_RemoveDirectoriesProcess();

    void ReadOutput_ListDirectoriesProcess();
    static void ReadOutput_RemoveDirectoriesProcess();

    void ShowDirectoryList(int eExitCode, QProcess::ExitStatus eExitStatus);
    void CleanUpAfterRemoveDirectories(int eExitCode, QProcess::ExitStatus eExitStatus);

private:
    QStringList GetSelectedDirectories(gpDirectoryTree *node);
    void ListDirectories();
    void RemoveSelectedDirectories();
    void UpdateDirectoryTree(const QString &string);

    gpDirectoryTree m_DirectoryTree;
    std::shared_ptr<QWidget> m_DirectoryWidgetsContainer = nullptr;
    QProcess *m_Process = nullptr;
    bool m_RemovalInProgress = false;
    QStringList m_ToBeRemoved;
    QString m_WorkingDirectory; ///< The directory containing the git repository that is selected for cleanup.

    Ui::gpDialog_RemoveDirectories *ui = nullptr; ///< The generated UI.
};

#endif // GPDIALOG_REMOVEDIRECTORIES_H
