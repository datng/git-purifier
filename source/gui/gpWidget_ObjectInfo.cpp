/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpWidget_ObjectInfo.cpp
 * @brief This file contains the implementation of the gpWidget_ObjectInfo class.
 */

#include "gpWidget_ObjectInfo.h"

#include "ui_gpWidget_ObjectInfo.h"

#include <utility>

gpWidget_ObjectInfo::gpWidget_ObjectInfo(gpObjectInfo info, QWidget *parent) //
    : QWidget(parent), Info(std::move(info)), ui(new Ui::gpWidget_ObjectInfo) {
    ui->setupUi(this);
}

gpWidget_ObjectInfo::~gpWidget_ObjectInfo() {
    delete ui;
}

void gpWidget_ObjectInfo::Fill(const gpObjectInfo &info) {
    ui->label_Size->setText(QString::number(info.Size));
    ui->label_File->setText(info.Path);
}

auto gpWidget_ObjectInfo::IsSelected() -> bool {
    return ui->checkBox->checkState() != 0U;
}
