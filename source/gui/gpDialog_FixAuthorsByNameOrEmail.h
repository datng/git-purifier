/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpDialog_FixAuthorsByNameOrEmail.h
 * @brief This file contains the declaration of the gpDialog_FixAuthorsByNameOrEmail class.
 */

#ifndef GPDIALOG_FIXAUTHORSBYNAMEOREMAIL_H
#define GPDIALOG_FIXAUTHORSBYNAMEOREMAIL_H

#include <QDialog>

#include "gpAuthorInfo.h"

#include <QProcess>
#include <QTableWidgetItem>

#include <memory>

namespace Ui {
class gpDialog_FixAuthorsByNameOrEmail;
}

namespace gp {
namespace gui {
class gpDialog_FixAuthorsByNameOrEmail : public QDialog {
    Q_OBJECT

public:
    explicit gpDialog_FixAuthorsByNameOrEmail(const QString &eWorkingDirectory); ///< The default constructor.
    ~gpDialog_FixAuthorsByNameOrEmail(); ///< The default destructor.

protected:
    void resizeEvent(QResizeEvent *e) override;

private slots:
    static void on_readyReadStandardError();
    static void on_FixFinished();
    void on_ListFinished();
    void on_tableWidget_AuthorList_cellChanged(int row, int column);

    void on_pushButton_Fix_clicked();

private:
    void m_ListAuthors();
    void m_FixAuthors();

    bool m_Editing = false;
    std::vector<gpAuthorInfo> m_AuthorInfoList;

    /**
     * @brief m_AuthorInfoFixMap
     * For each pair in this list, the author in the first element is replaced by that in the second
     */
    std::vector<std::pair<gpAuthorInfo, gpAuthorInfo>> m_AuthorInfoFixList;
    QProcess m_Process;
    std::vector<std::array<std::shared_ptr<QTableWidgetItem>, 4>> m_TableItems;
    QString m_WorkingDirectory; ///< The directory containing the git repository that is selected for change.

    Ui::gpDialog_FixAuthorsByNameOrEmail *ui = nullptr;
};
} // namespace gui
} // namespace gp

#endif // GPDIALOG_FIXAUTHORSBYNAMEOREMAIL_H
