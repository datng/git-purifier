/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpDialog_About.h
 * @brief This file contains the declaration of the gpDialog_About class.
 */

#ifndef GPDIALOG_ABOUT_H
#define GPDIALOG_ABOUT_H

#include <QDialog>

namespace Ui {
class gpDialog_About;
}

namespace gp {
namespace gui {
/**
 * @brief The gpDialog_About class describes the About dialog.
 */
class gpDialog_About : public QDialog {
    Q_OBJECT

public:
    explicit gpDialog_About(QWidget *parent = 0); ///< The default constructor.
    ~gpDialog_About(); ///< The default destructor.

private:
    Ui::gpDialog_About *ui; ///< The generated UI.
};
} // namespace gui
} // namespace gp
#endif // GPDIALOG_ABOUT_H
