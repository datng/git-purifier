/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpWidget_DirectoryTree.h
 * @brief This file contains the declaration of the gpWidget_DirectoryTree class.
 */

#ifndef GPWIDGET_DIRECTORYTREE_H
#define GPWIDGET_DIRECTORYTREE_H

#include <QWidget>

namespace Ui {
class gpWidget_DirectoryTreeEntry;
}

/**
 * @brief The gpWidget_DirectoryTree class describes the widget representing a gpDirectoryTree.
 */
class gpWidget_DirectoryTree : public QWidget {
    Q_OBJECT

public:
    /**
     * @brief gpWidget_DirectoryTree is the default constructor.
     * @param eDir is the path to the directory that this widget represents.
     * @param eLevel is the depth of this directory compared to the repository's root.
     * @param parent is the parent widget in which this widget resides.
     */
    explicit gpWidget_DirectoryTree(const QString &eDir, unsigned int eLevel, QWidget *parent);
    ~gpWidget_DirectoryTree(); ///< The default destructor.

    Qt::CheckState CheckState();
    void SetCheckState(Qt::CheckState eState);

    void SetDirectory(const QString &eDir); ///< Changes the label's text to match eDir.
    void SetLevel(unsigned int eLevel); ///< Changes the label's position to match eLevel.

signals:
    //! whereas eState is one of Qt::checkState enumerations
    void CheckBoxClicked(int eState, bool propagateUp, bool propagateDown);

private slots:
    void ToggleChecks(bool checked);

private:
    Ui::gpWidget_DirectoryTreeEntry *ui; ///< The generated UI.
};

#endif // GPWIDGET_DIRECTORYTREE_H
