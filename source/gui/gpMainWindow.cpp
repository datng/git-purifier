﻿/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpMainWindow.cpp
 * @brief This file contains the implementation of the gpMainWindow class.
 */

#include "gpMainWindow.h"
#include "ui_gpMainWindow.h"

#include "gpDialog_About.h"
#include "gpDialog_CleanBigFiles.h"
#include "gpDialog_FixAuthorsByNameOrEmail.h"
#include "gpDialog_RemoveDirectories.h"
#include "gpUtils.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QTextBlock>
#include <QTextCursor>
#include <QTimer>

namespace gp::gui {
gpMainWindow::gpMainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);

    m_WorkingDirectory = m_Program.WorkingDirectory().absolutePath();
    ui->lineEdit_Repository->setText(m_WorkingDirectory);
    ui->statusBar->showMessage("Working directory: " + m_WorkingDirectory);

    connect(ui->actionAboutGitPurifier, &QAction::triggered, this, &gpMainWindow::ShowDialog_About);
    connect(ui->actionExit, &QAction::triggered, this, [this] {
        gpProgram::SaveWorkingDirectory(m_WorkingDirectory);
        close();
    });
    connect(ui->pushButton_CleanBigFiles, &QPushButton::clicked, this, &gpMainWindow::ShowDialog_CleanBigFiles);
    connect(
        ui->pushButton_FixAuthorsByNameEmail,
        &QPushButton::clicked,
        this,
        &gpMainWindow::ShowDialog_FixAuthorsByNameEmail);
    connect(ui->pushButton_RemoveDirectories, &QPushButton::clicked, this, &gpMainWindow::ShowDialog_RemoveDirectories);

    CountObjects();
}

gpMainWindow::~gpMainWindow() {
    delete ui;
}

void gpMainWindow::ChooseRepository() {
    m_WorkingDirectory = QFileDialog::getExistingDirectory(
        nullptr, //
        "Choose Repository",
        m_WorkingDirectory,
        QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks //
    );
    ui->lineEdit_Repository->setText(m_WorkingDirectory);
    ui->statusBar->showMessage("Working directory: " + m_WorkingDirectory);
}

void gpMainWindow::CountObjects() {
    QProcess process;
    connect(&process, &QProcess::readyReadStandardOutput, this, &gpMainWindow::on_CountObjectsOutput);
    connect(&process, &QProcess::readyReadStandardError, this, &gpMainWindow::on_CountObjectsError);
    process.setWorkingDirectory(m_WorkingDirectory);
    gpCommand command = gpCommand::CountObjects();
    process.start(command.Program, command.Arguments);
    process.waitForFinished();
    disconnect(&process, &QProcess::readyReadStandardOutput, this, &gpMainWindow::on_CountObjectsOutput);
    disconnect(&process, &QProcess::readyReadStandardError, this, &gpMainWindow::on_CountObjectsError);
}

void gpMainWindow::ShowDialog_About() {
    auto *dialog = new gpDialog_About(this);
    dialog->setAttribute(Qt::WidgetAttribute::WA_DeleteOnClose);
    dialog->setModal(true);
    dialog->show();
}

void gpMainWindow::on_CountObjectsError() {
    QString err(dynamic_cast<QProcess *>(QObject::sender())->readAllStandardError());
    QMessageBox mes(QMessageBox::Icon::Critical, "Error", "Error: git count-objects error: " + err);
    mes.setModal(true);
    mes.exec();
}

void gpMainWindow::on_CountObjectsOutput() {
    QString output(dynamic_cast<QProcess *>(QObject::sender())->readAllStandardOutput());
    // outputList is a string list with 9 elements, with the last element being an empty string due to the last \n
    QStringList outputList = output.split('\n');

    if (outputList.size() == 9) {
        ui->label_Count_Loose->setText("Number of loose objects: " + outputList[0].split(':')[1]);
        ui->label_Size_Loose->setText("Loose objects size: " + outputList[1].split(':')[1]);
        ui->label_Count_InPack->setText("Number of in-pack objects: " + outputList[2].split(':')[1]);
        ui->label_Count_Pack->setText("Number of packs: " + outputList[3].split(':')[1]);
        ui->label_Size_Pack->setText("In-pack object size: " + outputList[4].split(':')[1]);
        ui->label_Count_PrunePackable->setText("Number of prune-packable objects: " + outputList[5].split(':')[1]);
        ui->label_Count_Garbage->setText("Number of garbarge objects: " + outputList[6].split(':')[1]);
        ui->label_Size_Garbage->setText("Garbarge objects size: " + outputList[7].split(':')[1]);
    }
}

void gpMainWindow::ShowDialog_CleanBigFiles() {
    auto *dialog = new gpDialog_CleanBigFiles(m_WorkingDirectory);
    dialog->setAttribute(Qt::WidgetAttribute::WA_DeleteOnClose);
    dialog->setModal(true);
    dialog->show();
}

void gpMainWindow::ShowDialog_FixAuthorsByNameEmail() {
    auto *dialog = new gpDialog_FixAuthorsByNameOrEmail(m_WorkingDirectory);
    dialog->setAttribute(Qt::WidgetAttribute::WA_DeleteOnClose);
    dialog->setModal(true);
    dialog->show();
}

void gpMainWindow::ShowDialog_RemoveDirectories() {
    auto *dialog = new gpDialog_RemoveDirectories(m_WorkingDirectory);
    dialog->setAttribute(Qt::WidgetAttribute::WA_DeleteOnClose);
    dialog->setModal(true);
    dialog->show();
}

void gpMainWindow::on_toolButton_Open_clicked() {
    ChooseRepository();
    CountObjects();
}
} // namespace gp::gui
