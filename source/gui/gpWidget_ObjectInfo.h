/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpWidget_ObjectInfo.h
 * @brief This file contains the declaration of the gpWidget_ObjectInfo class.
 */

#ifndef GPWIDGET_OBJECTINFO_H
#define GPWIDGET_OBJECTINFO_H
#include <QWidget>

#include "gpCommon.h"
#include "gpObjectInfo.h"

namespace Ui {
class gpWidget_ObjectInfo;
}

/**
 * @brief The gpWidget_ObjectInfo class describes the widget representing a gpObjectInfo.
 * This widget consists of a check box and 2 labels for file size and name.
 */
class gpWidget_ObjectInfo : public QWidget {
    Q_OBJECT

public:
    /**
     * @brief gpWidget_ObjectInfo is the default constructor.
     * @param info the gpObjectInfo associated with an object in a git repository.
     * @param parent is the parent widget.
     */
    explicit gpWidget_ObjectInfo(gpObjectInfo info, QWidget *parent = nullptr);
    ~gpWidget_ObjectInfo(); ///< The default destructor.

    void Fill(const gpObjectInfo &info); ///< Fills the widget with information from *info*
    bool IsSelected(); ///< Returns true if the checkbox is checked.

    gpObjectInfo Info; ///< The gpObjectInfo of the associated object in a git repository.

private:
    Ui::gpWidget_ObjectInfo *ui; ///< The generated UI.
};

#endif // GPWIDGET_OBJECTINFO_H
