﻿/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpDialog_CleanBigFiles.cpp
 * @brief This file contains the implementation of the gpDialog_CleanBigFiles class.
 */

#include "gpDialog_CleanBigFiles.h"
#include "ui_gpDialog_CleanBigFiles.h"

#include "gpCommand.h"
#include "gpUtils.h"

#include <QMessageBox>
#include <QProcess>
#include <QResizeEvent>

#include <iostream>

gpDialog_CleanBigFiles::gpDialog_CleanBigFiles(const QString &eWorkingDirectory) //
    : QDialog(nullptr), ui(new Ui::gpDialog_CleanBigFiles) {
    ui->setupUi(this);
    m_WorkingDirectory = eWorkingDirectory;
    m_Heading = new gpWidget_ObjectInfo(gpObjectInfo(), this);
    m_Heading->setGeometry(10, 10, m_Heading->width(), m_Heading->height());
    m_InfoWidgetsContainer = new QWidget();
    ui->scrollArea->setWidget(m_InfoWidgetsContainer);
    ListObjectsBySize();
}

gpDialog_CleanBigFiles::~gpDialog_CleanBigFiles() {
    delete m_Heading;
    delete ui;
    for (auto *infoWidget : m_InfoWidgets) { delete infoWidget; }
    while (!m_InfoWidgets.empty()) { m_InfoWidgets.pop_back(); }
}

void gpDialog_CleanBigFiles::ListObjectsBySize() {
    m_Infos.clear();
    auto command = gpCommand::ListObjectsBySize(gp::gpConfigDir());
    m_Process.setWorkingDirectory(m_WorkingDirectory);
    m_Process.start(command.Program, command.Arguments);
    connect(&m_Process, &QProcess::readyReadStandardOutput, this, &gpDialog_CleanBigFiles::on_readyReadStandardOutput);
    connect(&m_Process, &QProcess::readyReadStandardError, this, &gpDialog_CleanBigFiles::on_readyReadStandardError);
    connect(
        &m_Process,
        QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
        this,
        &gpDialog_CleanBigFiles::on_ListFinished);
}

void gpDialog_CleanBigFiles::on_readyReadStandardError() {
    auto error = m_Process.readAllStandardError();
    QMessageBox mes(QMessageBox::Icon::Critical, "Error", error);
    mes.setModal(true);
    mes.exec();
}

void gpDialog_CleanBigFiles::on_readyReadStandardOutput() {
    while (m_Process.canReadLine()) {
        QString string = m_Process.readLine();
        auto info = gpObjectInfo::Parse(m_WorkingDirectory, string);
        bool alreadyexist = false;
        for (auto &i : m_Infos) {
            if (i.Path == info.Path) {
                alreadyexist = true;
                if (info.Size > i.Size) { i.Size = info.Size; }
                break;
            }
        }
        if (!alreadyexist) { m_Infos.push_back(info); }
    }
}

void gpDialog_CleanBigFiles::on_ListFinished() {
    disconnect(
        &m_Process, &QProcess::readyReadStandardOutput, this, &gpDialog_CleanBigFiles::on_readyReadStandardOutput);
    disconnect(
        &m_Process,
        QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
        this,
        &gpDialog_CleanBigFiles::on_ListFinished);

    if (!m_InfoWidgets.empty()) {
        for (auto &widget : m_InfoWidgets) { delete widget; }
    }
    m_InfoWidgets.clear();

    for (auto &info : m_Infos) {
        auto *infoWidget = new gpWidget_ObjectInfo(info, m_InfoWidgetsContainer);
        infoWidget->Fill(info);
        QPalette p;
        if ((m_InfoWidgets.size() % 2) != 0U) {
            p.setColor(QPalette::Window, QColor(255, 255, 255));
        } else {
            p.setColor(QPalette::Window, QColor(244, 244, 244));
        }

        infoWidget->setAutoFillBackground(true);
        infoWidget->setPalette(p);

        // y is the sum of all the info widgets' heights.
        // which is used as the starting vertical position for the newly created info widget
        int y = static_cast<int>(m_InfoWidgets.size()) * infoWidget->height();
        infoWidget->setGeometry(infoWidget->x(), y, infoWidget->width(), infoWidget->height());
        infoWidget->show();
        m_InfoWidgets.push_back(infoWidget);
    }

    // WORKAROUND
    // without these, the scrollbar will not show up
    resize(this->size().width(), this->size().height() + 10);
    resize(this->size().width(), this->size().height() - 10);
}

void gpDialog_CleanBigFiles::resizeEvent(QResizeEvent *eEvent) {
    ui->scrollArea->setGeometry(10, 45, eEvent->size().width() - 120, eEvent->size().height() - 55);
    ui->line->setGeometry(10, 40, eEvent->size().width() - 120, ui->line->height());

    int containerHeight = static_cast<int>(m_InfoWidgets.size()) * 30;
    m_InfoWidgetsContainer->setMinimumSize(eEvent->size().width() - 145, containerHeight);
    m_InfoWidgetsContainer->setGeometry(0, 0, eEvent->size().width() - 145, containerHeight);

    ui->pushButton_Remove->setGeometry(eEvent->size().width() - 100, 10, 90, 30);
}

void gpDialog_CleanBigFiles::on_pushButton_Remove_clicked() {
    for (auto &widget : m_InfoWidgets) {
        if (widget->IsSelected()) { m_ToBeRemoved.append(widget->Info.Path); }
    }
    if (m_ToBeRemoved.empty()) { return; }
    RemoveSelectedFiles();
}

void gpDialog_CleanBigFiles::RemoveSelectedFiles() {
    if (!m_ToBeRemoved.isEmpty()) {
        this->setEnabled(false);
        m_RemovalInProgress = true;
        gpCommand command = gpCommand::RemoveFile(m_ToBeRemoved[0]);
        std::cout //
            << command.Arguments[0].toStdString() << " " << command.Arguments[1].toStdString() << " "
            << command.Arguments[2].toStdString() << std::endl;
        auto *removalprocess = new QProcess;
        removalprocess->setWorkingDirectory(m_WorkingDirectory);

        removalprocess->start(command.Program, command.Arguments);
        connect(removalprocess, &QProcess::readyRead, this, [removalprocess]() {
            std::cout << QString(removalprocess->readAll()).toStdString() << std::endl;
        });
        connect(
            removalprocess,
            QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            this,
            &gpDialog_CleanBigFiles::on_RemovalProcessFinished);
    } else {
        this->setEnabled(true);
        m_RemovalInProgress = false;
        ListObjectsBySize();
    }
}

void gpDialog_CleanBigFiles::on_RemovalProcessFinished(int  /*eExitCode*/, QProcess::ExitStatus eExitStatus) {
    disconnect(QObject::sender());
    delete QObject::sender();
    std::cout << "Process ended with status: " << eExitStatus << std::endl;
    m_ToBeRemoved.removeFirst();
    RemoveSelectedFiles();
}

void gpDialog_CleanBigFiles::reject() {
    if (m_RemovalInProgress) { return; }
    QDialog::reject();
}
