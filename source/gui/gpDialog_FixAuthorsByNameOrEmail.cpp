/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpDialog_FixAuthorsByNameOrEmail.cpp
 * @brief This file contains the implementation of the gpDialog_FixAuthorsByNameOrEmail class.
 */

#include "gpDialog_FixAuthorsByNameOrEmail.h"
#include "ui_gpDialog_FixAuthorsByNameOrEmail.h"

#include "gpCommand.h"
#include "gpCommon.h"

#include <QMessageBox>
#include <QResizeEvent>

#include <iostream>

namespace gp::gui {
gpDialog_FixAuthorsByNameOrEmail::gpDialog_FixAuthorsByNameOrEmail(const QString &eWorkingDirectory)
    : QDialog(nullptr), ui(new Ui::gpDialog_FixAuthorsByNameOrEmail) {
    ui->setupUi(this);

    m_WorkingDirectory = eWorkingDirectory;

    m_Process.setWorkingDirectory(m_WorkingDirectory);
    connect(
        &m_Process,
        &QProcess::readyReadStandardError,
        this,
        &gpDialog_FixAuthorsByNameOrEmail::on_readyReadStandardError);
    m_ListAuthors();
}

gpDialog_FixAuthorsByNameOrEmail::~gpDialog_FixAuthorsByNameOrEmail() {
    m_TableItems.clear();
    delete ui;
}

void gpDialog_FixAuthorsByNameOrEmail::m_FixAuthors() {
    for (auto &pair : m_AuthorInfoFixList) {
        auto command = gpCommand::FixAuthorNamesOrEmails(
            pair.first.Email(), pair.first.Name(), pair.second.Email(), pair.second.Name());

        m_Process.setWorkingDirectory(m_WorkingDirectory);
        connect( //
            &m_Process,
            &QProcess::readyRead,
            this,
            [this]() { std::cout << QString(this->m_Process.readAll()).toStdString() << std::endl; } //
        );
        connect(
            &m_Process,
            QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            this,
            &gpDialog_FixAuthorsByNameOrEmail::on_FixFinished);
        m_Process.start(command.Program, command.Arguments);
    }
}

void gpDialog_FixAuthorsByNameOrEmail::m_ListAuthors() {
    gpCommand command = gpCommand::ListAuthors();
    connect(
        &m_Process,
        QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
        this,
        &gpDialog_FixAuthorsByNameOrEmail::on_ListFinished);
    m_Process.start(command.Program, command.Arguments);
}

void gpDialog_FixAuthorsByNameOrEmail::on_FixFinished() {
    std::cout << "Fixing done" << std::endl;
}

void gpDialog_FixAuthorsByNameOrEmail::on_ListFinished() {
    disconnect(
        &m_Process,
        QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
        this,
        &gpDialog_FixAuthorsByNameOrEmail::on_ListFinished);

    QString output = m_Process.readAllStandardOutput();
    QStringList author_list = output.split("\n");
    author_list.removeAll("");

    m_AuthorInfoList.clear();
    for (auto &author : author_list) {
        gpAuthorInfo info(author);
        if (info.IsValid()) { m_AuthorInfoList.push_back(info); }
    }

    // If the dialog is closed while the list author process is still running,
    // the process will be terminated, and emit the finished signal.
    // This function will be run before the dialog actually closes.
    if (m_AuthorInfoList.empty()) { return; }
    ui->tableWidget_AuthorList->setRowCount(static_cast<int>(m_AuthorInfoList.size()));

    // create table widget items
    // but first clear out old ones if exist
    m_TableItems.clear();
    for (size_t i = 0; i < m_AuthorInfoList.size(); ++i) {
        m_TableItems.emplace_back();
        auto item_Name_original = std::make_shared<QTableWidgetItem>(m_AuthorInfoList[i].Name());
        item_Name_original->setFlags(Qt::ItemFlag::ItemIsSelectable | Qt::ItemFlag::ItemIsEnabled);
        auto item_Name_fixed = std::make_shared<QTableWidgetItem>(m_AuthorInfoList[i].Name());

        auto item_Email_original = std::make_shared<QTableWidgetItem>(m_AuthorInfoList[i].Email());
        item_Email_original->setFlags(Qt::ItemFlag::ItemIsSelectable | Qt::ItemFlag::ItemIsEnabled);
        auto item_Email_fixed = std::make_shared<QTableWidgetItem>(m_AuthorInfoList[i].Email());

        m_TableItems[i][0] = item_Name_original;
        ui->tableWidget_AuthorList->setItem(static_cast<int>(i), 0, item_Name_original.get());
        m_TableItems[i][1] = item_Email_original;
        ui->tableWidget_AuthorList->setItem(static_cast<int>(i), 1, item_Email_original.get());

        m_TableItems[i][2] = item_Name_fixed;
        ui->tableWidget_AuthorList->setItem(static_cast<int>(i), 2, item_Name_fixed.get());
        m_TableItems[i][3] = item_Email_fixed;
        ui->tableWidget_AuthorList->setItem(static_cast<int>(i), 3, item_Email_fixed.get());
    }

    ui->tableWidget_AuthorList->resizeColumnsToContents();
}

void gpDialog_FixAuthorsByNameOrEmail::resizeEvent(QResizeEvent *e) {
    const int margin(10);
    const int marginHorizontal(margin * 2); // 10 pixels each side
    const int marginVertical(margin * 3); // 10 pixels top and bottom + 10 pixels between the tab widget and the button
    ui->tableWidget_AuthorList->resize( //
        e->size().width() - marginHorizontal,
        e->size().height() - ui->pushButton_Fix->height() - marginVertical);

    ui->pushButton_Fix->setGeometry(
        e->size().width() - margin - ui->pushButton_Fix->width(),
        e->size().height() - margin - ui->pushButton_Fix->height(),
        ui->pushButton_Fix->width(),
        ui->pushButton_Fix->height());
}

void gp::gui::gpDialog_FixAuthorsByNameOrEmail::on_pushButton_Fix_clicked() {
    auto pairCount = 0U;
    for (auto &pair : m_AuthorInfoFixList) { ++pairCount; }
    QString temp;
    for (auto &pair : m_AuthorInfoFixList) {
        temp += "Original author: " + pair.first.Name() + ", " + pair.first.Email() +
                ". Fixed author: " + pair.second.Name() + ", " + pair.second.Email() + "\n";
    }
    QMessageBox mb(
        QMessageBox::Icon::Information,
        "Info",
        "Number of authors that required fixing: " + QString::number(pairCount) + "\n" + temp);
    mb.setModal(true);
    mb.exec();

    m_FixAuthors();
}

void gpDialog_FixAuthorsByNameOrEmail::on_readyReadStandardError() {
    TODO;
}

void gpDialog_FixAuthorsByNameOrEmail::on_tableWidget_AuthorList_cellChanged(int row, int column) {
    if (m_Editing) { return; }
    if (column == 2 || column == 3) {
        auto *authorTable = ui->tableWidget_AuthorList;
        if (authorTable->item(row, column)->text() == authorTable->item(row, column - 2)->text() //
            || authorTable->item(row, column)->text().trimmed() == "") {
            // nothing has changed, reset the cell
            m_Editing = true;
            authorTable->item(row, column)->setBackground(QBrush(this->palette().base()));
            authorTable->item(row, column)->setForeground(QBrush(this->palette().text()));
            authorTable->item(row, column)->setText("");
            m_Editing = false;
            authorTable->item(row, column - 2)->setBackground(QBrush(this->palette().base()));
            authorTable->item(row, column - 2)->setForeground(QBrush(this->palette().text()));
            return;
        }

        // something changed, fix the color of the cell
        m_Editing = true;
        authorTable->item(row, column)->setBackground(QBrush(Qt::darkGreen));
        authorTable->item(row, column)->setForeground(QBrush(this->palette().brightText()));
        m_Editing = false;
        authorTable->item(row, column - 2)->setBackground(QBrush(Qt::darkRed));
        authorTable->item(row, column - 2)->setForeground(QBrush(this->palette().brightText()));

        auto oldName = authorTable->item(row, 0)->text();
        auto oldEmail = authorTable->item(row, 1)->text();
        auto newName = authorTable->item(row, 2)->text();
        auto newEmail = authorTable->item(row, 3)->text();

        gpAuthorInfo original(oldName, oldEmail);
        gpAuthorInfo fixed(newName, newEmail);
        if (original.IsValid()) {
            if (fixed.IsValid()) {
                bool isAlreadyInList = false;
                for (auto &temp : m_AuthorInfoFixList) {
                    if (temp.first == original) {
                        isAlreadyInList = true;
                        temp.second = fixed;
                    }
                }
                if (!isAlreadyInList) { m_AuthorInfoFixList.emplace_back(std::make_pair(original, fixed)); }
            }
        }
    }
    ui->tableWidget_AuthorList->resizeColumnsToContents();
}
} // namespace gp::gui
