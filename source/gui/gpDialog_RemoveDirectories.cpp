/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpDialog_RemoveDirectories.cpp
 * @brief This file contains the implementation of the gpDialog_RemoveDirectories class.
 */

#include "gpDialog_RemoveDirectories.h"
#include "ui_gpDialog_RemoveDirectories.h"

#include "gpCommand.h"
#include "gpUtils.h"

#include <QResizeEvent>

#include "iostream"

gpDialog_RemoveDirectories::gpDialog_RemoveDirectories(const QString &eWorkingDirectory)
    : QDialog(nullptr), ui(new Ui::gpDialog_RemoveDirectories) {
    ui->setupUi(this);
    m_WorkingDirectory = eWorkingDirectory;

    m_DirectoryWidgetsContainer = std::make_shared<QWidget>();
    ui->scrollArea->setWidget(m_DirectoryWidgetsContainer.get());
    m_Process = new QProcess();
    connect(
        m_Process,
        &QProcess::readyReadStandardOutput,
        this,
        &gpDialog_RemoveDirectories::ReadOutput_ListDirectoriesProcess);
    connect(
        m_Process,
        QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
        this,
        &gpDialog_RemoveDirectories::ShowDirectoryList);
    ListDirectories();
}

gpDialog_RemoveDirectories::~gpDialog_RemoveDirectories() {
    delete m_Process;
    delete ui;
}

void gpDialog_RemoveDirectories::reject() {
    if (m_RemovalInProgress) { return; }
    QDialog::reject();
}

void gpDialog_RemoveDirectories::resizeEvent(QResizeEvent *eEvent) {
    ui->scrollArea->setGeometry( //
        10,
        10,
        eEvent->size().width() - 120,
        eEvent->size().height() - 20);
    int containerHeight = m_DirectoryTree.CountItems() * 30;
    m_DirectoryWidgetsContainer->setMaximumHeight(containerHeight);
    m_DirectoryWidgetsContainer->setMinimumHeight(containerHeight);
    ui->pushButton_Remove->setGeometry( //
        eEvent->size().width() - 100,
        10,
        ui->pushButton_Remove->width(),
        ui->pushButton_Remove->height());
}

void gpDialog_RemoveDirectories::on_pushButton_Remove_clicked() {
    m_ToBeRemoved = GetSelectedDirectories(&m_DirectoryTree);
    RemoveSelectedDirectories();
}

void gpDialog_RemoveDirectories::ReadError_ListDirectoriesProcess() {
    TODO;
}

void gpDialog_RemoveDirectories::ReadError_RemoveDirectoriesProcess() {
    TODO;
}

void gpDialog_RemoveDirectories::ReadOutput_ListDirectoriesProcess() {
    while (m_Process->canReadLine()) {
        auto string = m_Process->readLine();
        if (string.contains("/")) { UpdateDirectoryTree(string); }
    }
}

void gpDialog_RemoveDirectories::ReadOutput_RemoveDirectoriesProcess() {
    TODO;
}

void gpDialog_RemoveDirectories::ShowDirectoryList(int /*eExitCode*/, QProcess::ExitStatus /*eExitStatus*/) {
    disconnect(QObject::sender());
    m_DirectoryTree.UpdateWidgetsContent(m_DirectoryWidgetsContainer.get());
    int i = 0;
    m_DirectoryTree.UpdateWidgetsPlacement(i);
    m_DirectoryTree.ShowWidgets();

    ui->pushButton_Remove->setEnabled(true);

    // WORKAROUND
    // without these, the scrollbar will not show up
    resize(this->size().width(), this->size().height() + 10);
    resize(this->size().width(), this->size().height() - 10);
}

void gpDialog_RemoveDirectories::CleanUpAfterRemoveDirectories(int /*eExitCode*/, QProcess::ExitStatus eExitStatus) {
    disconnect(QObject::sender());
    delete QObject::sender();
    std::cout << "Process ended with status: " << eExitStatus << std::endl;
}

auto gpDialog_RemoveDirectories::GetSelectedDirectories(gpDirectoryTree *node) -> QStringList {
    QStringList dirs;
    if (node->Widget->CheckState() == Qt::CheckState::Checked) {
        dirs.append(node->Path());
    } else {
        for (auto &child : node->SubTrees) { dirs.append(GetSelectedDirectories(child.get())); }
    }
    return dirs;
}

void gpDialog_RemoveDirectories::ListDirectories() {
    ui->pushButton_Remove->setEnabled(false);
    m_DirectoryTree.Clear();
    m_DirectoryTree.UpdateWidgetsContent(m_DirectoryWidgetsContainer.get());
    gpCommand command = gpCommand::ListObjectsByName(gp::gpConfigDir());
    m_Process->setWorkingDirectory(m_WorkingDirectory);

    m_Process->start(command.Program, command.Arguments);
}

void gpDialog_RemoveDirectories::RemoveSelectedDirectories() {
    while (!m_ToBeRemoved.empty()) {
        this->setEnabled(false);
        m_RemovalInProgress = true;
        gpCommand command = gpCommand::RemoveDirectory(m_ToBeRemoved[0]);
        std::cout //
            << command.Arguments[0].toStdString() << " " << command.Arguments[1].toStdString() << " "
            << command.Arguments[2].toStdString() << std::endl;
        auto *removalprocess = new QProcess;
        removalprocess->setWorkingDirectory(m_WorkingDirectory);
        connect( //
            removalprocess,
            &QProcess::readyRead,
            this,
            [removalprocess]() {
                std::cout //
                    << QString(removalprocess->readAll()).toStdString() << std::endl;
            } //
        );
        connect(
            removalprocess,
            QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            this,
            &gpDialog_RemoveDirectories::CleanUpAfterRemoveDirectories);

        removalprocess->start(command.Program, command.Arguments);
        m_ToBeRemoved.removeFirst();
    }

    this->setEnabled(true);
    m_RemovalInProgress = false;
    ListDirectories();
}

void gpDialog_RemoveDirectories::UpdateDirectoryTree(const QString &string) {
    QStringList list = string.split("/");
    list[0] = list[0].split("\t")[1];
    gpDirectoryTree *tree = &m_DirectoryTree;
    for (int i = 0; i < list.size() - 1; ++i) {
        bool branchExists = false;
        for (auto &branch : tree->SubTrees) {
            if (branch->Name == list[i]) {
                tree = branch.get();
                branchExists = true;
            }
        }
        if (!branchExists) {
            auto branch = std::make_shared<gpDirectoryTree>(list[i], tree);
            tree->SubTrees.append(branch);
            tree->SortAlphabetical();
            tree = branch.get();
        }
    }
}
