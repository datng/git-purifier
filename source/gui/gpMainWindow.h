﻿/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpMainWindow.h
 * @brief This file contains the declaration of the gpMainWindow class.
 */

#ifndef GPMAINWINDOW_H
#define GPMAINWINDOW_H

#include <QMainWindow>

#include "gpCommand.h"
#include "gpProgram.h"

#include <QProcess>
#include <QQueue>

namespace Ui {
class MainWindow;
}

namespace gp {
namespace gui {
/**
 * @brief The gpMainWindow class describes the main window.
 * It is the window that is opened when the program starts.
 */
class gpMainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit gpMainWindow(QWidget *parent = nullptr); ///< The default constructor.
    ~gpMainWindow(); ///< The default destructor.

private slots:
    void ShowDialog_About(); ///< Opens the About dialog.
    void ShowDialog_CleanBigFiles(); ///< Opens the Clean Big Files dialog.
    void ShowDialog_FixAuthorsByNameEmail();
    void ShowDialog_RemoveDirectories(); ///< Opens the Remove Directories dialog.

    /**
     * @brief on_toolButton_Open_clicked opens a file dialog
     * and lets users select a directory containing a git repository to clean up,
     * then executes "git count-objects -vH" in that directory and updates the UI based on its output.
     * @sa m_WorkingDirectory
     */
    void on_toolButton_Open_clicked();

    /**
     * @brief on_CountObjectsError displays an error message when there is an error during CountObjects.
     * @sa CountObjects
     */
    void on_CountObjectsError();

    /**
     * @brief on_CountObjectsOutput updates the UI based on the output of the process in CountObjects.
     * @sa CountObjects
     */
    void on_CountObjectsOutput();

private:
    /**
     * @brief ChooseRepository opens a file dialog
     * and lets users select a directory containing a git repository to clean up.
     * @sa m_WorkingDirectory
     */
    void ChooseRepository();

    /**
     * @brief CountObjects executes "git count-objects -vH"
     * and updates the UI based on its output.
     */
    void CountObjects();

    gpProgram m_Program;
    QString m_WorkingDirectory; ///< The directory containing the git repository that is selected for cleanup.

    Ui::MainWindow *ui = nullptr; ///< The generated UI.
};
} // namespace gui
} // namespace gp
#endif // GPMAINWINDOW_H
