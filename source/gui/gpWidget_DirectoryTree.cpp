/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file gpWidget_DirectoryTree.cpp
 * @brief This file contains the implementation of the gpWidget_DirectoryTree class.
 */

#include "gpWidget_DirectoryTree.h"
#include "ui_gpWidget_DirectoryTree.h"

gpWidget_DirectoryTree::gpWidget_DirectoryTree(const QString &eDir, unsigned int eLevel, QWidget *parent)
    : QWidget(parent), ui(new Ui::gpWidget_DirectoryTreeEntry) {
    ui->setupUi(this);

    connect(ui->checkBox, &QCheckBox::clicked, this, &gpWidget_DirectoryTree::ToggleChecks);
    SetDirectory(eDir);
    SetLevel(eLevel);
}

gpWidget_DirectoryTree::~gpWidget_DirectoryTree() {
    delete ui;
}

auto gpWidget_DirectoryTree::CheckState() -> Qt::CheckState {
    return ui->checkBox->checkState();
}

void gpWidget_DirectoryTree::SetCheckState(Qt::CheckState eState) {
    ui->checkBox->setCheckState(eState);
}

void gpWidget_DirectoryTree::SetDirectory(const QString &eDir) {
    ui->label->setText(eDir);
}

void gpWidget_DirectoryTree::SetLevel(unsigned int eLevel) {
    const int level0Indent(40);
    const int subsequentLevelIndent(30);
    ui->label->setGeometry( //
        level0Indent + subsequentLevelIndent * static_cast<int>(eLevel),
        ui->label->y(),
        ui->label->width(),
        ui->label->height());
}

void gpWidget_DirectoryTree::ToggleChecks(bool checked) {
    if (checked) {
        ui->checkBox->setCheckState(Qt::CheckState::Checked);
        emit CheckBoxClicked(Qt::CheckState::Checked, true, true);
    } else {
        ui->checkBox->setCheckState(Qt::CheckState::Unchecked);
        emit CheckBoxClicked(Qt::CheckState::Unchecked, true, true);
    }
}
