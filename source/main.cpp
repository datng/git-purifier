﻿/**
 * Git Purifier
 * Copyright (C) 2018-2019  Tien Dat Nguyen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file main.cpp
 * @brief This file contains main function.
 */

#include "gui/gpMainWindow.h" // TODO: fix relationship between these classes

#include <QApplication>

/**
 * @brief main is the main function
 * @param argc
 * @param argv
 * @return
 */
auto main(int argc, char *argv[]) -> int {
    QApplication a(argc, argv);
    gp::gui::gpMainWindow w;
    w.show();

    return QApplication::exec();
}
