doxygen Doxyfile

cd build

cd docs

python3 -m coverxygen --xml-dir xml --src-dir ../.. --output coverxygenated.info --kind all

genhtml --no-function-coverage --no-branch-coverage coverxygenated.info -o ./coverage

lcov --summary coverxygenated.info
